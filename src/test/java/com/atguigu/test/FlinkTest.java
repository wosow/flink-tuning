package com.atguigu.test;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class FlinkTest {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(4);

        DataStreamSource<String> textStream = env.socketTextStream("hadoop102", 9999);
        SingleOutputStreamOperator<String> map = textStream.map(new MapFunction<String, String>() {
            @Override
            public String map(String value) throws Exception {
                return value;
            }
        });

        map.print("Map>>>>>>");

//        textStream.rebalance();
//        textStream.rescale();
//        textStream.shuffle();

//        map.global().print("global>>>>>").setParallelism(4);
//        map.broadcast().print("broadcast>>>").setParallelism(4);
        map.forward().print().setParallelism(4);

        env.execute();

    }

}
