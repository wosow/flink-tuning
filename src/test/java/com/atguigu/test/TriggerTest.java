package com.atguigu.test;

import com.atguigu.bean.Bean1;
import com.atguigu.bean.MyTrigger;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

import java.time.Duration;

public class TriggerTest {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<String> dataStreamSource = env.socketTextStream("hadoop102", 9999);

        SingleOutputStreamOperator<Bean1> map = dataStreamSource.map(line -> {
            String[] fields = line.split(",");
            return new Bean1(fields[0], Long.parseLong(fields[1]), Long.parseLong(fields[2]));
        });

        SingleOutputStreamOperator<Bean1> andWatermarks = map.assignTimestampsAndWatermarks(WatermarkStrategy.<Bean1>forMonotonousTimestamps().withTimestampAssigner(new SerializableTimestampAssigner<Bean1>() {
            @Override
            public long extractTimestamp(Bean1 element, long recordTimestamp) {
                return element.getTs();
            }
        }));

        SingleOutputStreamOperator<Bean1> result = andWatermarks.keyBy(Bean1::getId)
                .window(TumblingEventTimeWindows.of(Time.seconds(5)))
                .trigger(new MyTrigger())
                .reduce(new ReduceFunction<Bean1>() {
                    @Override
                    public Bean1 reduce(Bean1 value1, Bean1 value2) throws Exception {
                        value1.setVc(value1.getVc() + value2.getVc());
                        return value1;
                    }
                });

        result.print(">>>>>>>>");

        env.execute();
    }

}
